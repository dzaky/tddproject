from django import forms
from .models import StatusAnda

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusAnda
        fields = ('status',)

        widgets = {
            'status': forms.TextInput(attrs={'class': 'form-control','type': 'text'})
        }

        labels = {
            'status': 'Status:',
        }

        error_messages = {
            'status': {'max_length': 'Terlalu banyak karakter yang dimasukan'},
        }