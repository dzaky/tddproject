
        $('document').ready(function() {
            var arrayTheme = [{background: '#008000', fontcolor: 'blue'}];
            var arrayTheme2 = [{background: '#b8860b', fontcolor: 'black'}]

            $('.test-jquery').click(function() {
                $('h1').css({'color': arrayTheme[0].fontcolor});
                $('h2').css({'color': arrayTheme[0].fontcolor});
                $('.row').css({'color': arrayTheme[0].fontcolor});
                $('body').css({'background-color': arrayTheme[1].background});
            });
            
            $('.test-jquery2').click(function() {
                $('h1').css({'color': arrayTheme2[0].fontcolor});
                $('h2').css({'color': arrayTheme2[0].fontcolor});
                $('.row').css({'color': arrayTheme2[0].fontcolor});
                $('body').css({'background-color': arrayTheme2[1].background});
            });

            $('.accordion').on('click', '.accordion-header', function() {
                $(this).toggleClass('active').next().slideToggle();
            });
        });

        var myVar;

        function myFunction() {
            myVar = setTimeout(showPage, 3000);
        }

        function showPage() {
            document.getElementById("loader").style.display = "none";
            document.getElementById("myDiv").style.display = "block";
        }