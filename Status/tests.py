from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import StatusAnda
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index_status
import time

# Create your tests here.
class Story6Test(TestCase):
	def test_url_exist(self):
		response = Client().get('/status')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/status')
		self.assertEqual(found.func, index_status)

	def test_landingpage_containt(self):
		response = self.client.get('/status')
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa Kabar?', html_response)

	def test_can_create_new_status(self):
		testText = 'Lorem Ipsum'
		new_act = StatusAnda.objects.create(status= testText)
		counting_all_available_status =  StatusAnda.objects.all().count();
		self.assertEqual(counting_all_available_status, 1)

	def test_form_Status_rendered(self):
		response = Client().get('/status')
		html_response = response.content.decode('utf8')
		self.assertIn('status', html_response)

#Challenge6
	def test_challenge6_url_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)

	def test_challenge6_html_exist(self):
		response = Client().get('/profile')
		self.assertTemplateUsed(response, 'profile.html')

	def test_profilepage_containt(self):
		response = self.client.get('/profile')
		html_response = response.content.decode('utf8')
		self.assertIn('Dzaky Abdi Al Jabbar', html_response)

#Story7
class Story7FunctionalTest(TestCase):

	# def test_theme(self):
	# 	selenium = self.selenium
	# 	selenium.get('https://dzakytddproject.herokuapp.com/profile/')
		
	# 	time.sleep(3)
	# 	buttonTheme1 = selenium.find_element_by_id('header1')
	# 	buttonTheme2 = selenium.find_element_by_id('header2')
		
	# 	tagname = selenium.find_element_by_tag_name('h1')

	# 	buttonTheme1.click()
	# 	testh1Change = tagname.value_of_css_property("color")
	# 	self.assertEqual('', testh1Change)



	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		service_log_path = "./chromedriver.log"
		service_args = ['--verbose']
		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		self.selenium.implicitly_wait(25)
		super(Story7FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story7FunctionalTest, self).tearDown()

	def test_input_todo(self):
		selenium = self.selenium
		
		# Opening the link we want to test
		selenium.get('https://dzakytddproject.herokuapp.com')
		time.sleep(2)

		# Find the form element
		status = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_tag_name('button')
		time.sleep(2)
		
		#Fill the form with data
		status.send_keys('Mengerjakan Story PPW')
		time.sleep(2)
		
		#Submitting the form
		submit.send_keys(Keys.RETURN)
		time.sleep(2)

		#Check
		self.assertIn('Mengerjakan Story PPW', selenium.page_source)

	def test_layout_position_title(self):
		selenium = self.selenium
		
		selenium.get('https://dzakytddproject.herokuapp.com/')
		time.sleep(3)
		self.assertIn("Status", selenium.title)

	def test_layout_position_daftar_status(self):
		selenium = self.selenium
		
		selenium.get("https://dzakytddproject.herokuapp.com/")
		header_text = selenium.find_element_by_tag_name('h3')
		time.sleep(3)
		self.assertIn("Daftar Status", selenium.page_source)

	def test_img_used_css_property(self):
		selenium = self.selenium

		selenium.get("https://dzakytddproject.herokuapp.com/profile")
		img = selenium.find_element_by_tag_name('img').value_of_css_property('border-radius')
		time.sleep(3)
		self.assertIn('50%', img)

	def test_background_used_css_property(self):
		selenium = self.selenium

		selenium.get('https://dzakytddproject.herokuapp.com/profile')
		img = selenium.find_element_by_tag_name('img').value_of_css_property('padding')
		time.sleep(3)
		self.assertIn('0px', img)

