from django.shortcuts import render
from .forms import StatusForm
from django.http import HttpResponseRedirect
from .models import StatusAnda

response = {}

def index_status(request):
    html = 'index.html'
    daftarstatus = StatusAnda.objects.all().values()
    response['daftarstatus']=daftarstatus

    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('status')
        else:
            form = StatusForm()
            return render(request, "status.html", {'form' : form})

    form = StatusForm()
    response['form']=form
    return render(request, 'status.html',response)

def index_show_status(request):
    temp = StatusAnda.objects.all()
    return render(request, 'status.html', {'table':temp})

def index_profile(request):
    return render(request, 'profile.html')