from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import list_buku
# Create your tests here.
class Story9Test(TestCase):
    # def test_html_exist(self):
    #     response = Client().get('/favbook')
    #     self.assertTemplateUsed(response, 'story9.html')

    def test_function_caller_exist(self):
        found = resolve('/favbook')
        self.assertEqual(found.func, list_buku)