from django.shortcuts import render
import json

# Create your views here.
BOOK_API = 'https://www.googleapis.com/books/v1/volumes?q=quilting'

response = {}

def list_buku(request):
    books = json.dumps(BOOK_API)
    response['books'] = books
    return render(request, 'story9.html', response)
