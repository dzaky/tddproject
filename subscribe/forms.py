from django import forms
from .models import Subscriber


class FormRegistrasi(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ('name','email','password')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':"What's your name?",'id':'name_form'}),
            'email': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':"What's your email?",'id':'email_form'}),
            'password': forms.TextInput(attrs={'class': 'form-control','type': 'password','placeholder':"Input your password",'id':'password_form'}),
        }

        labels = {
            'name': 'Full Name',
            'email': 'Email',
            'password': 'Password'
        }
