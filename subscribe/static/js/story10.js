var name_valid = false
var email_valid = false
var password_valid = false

function check_status() {
    if(name_valid && email_valid && password_valid) {
        $("#button_subs").removeAttr("disabled");
    } else {
        $("#button_subs").attr("disabled", "disabled");
    }
}

$("#name").change(function () {
    if(document.getElementById('name').value !== "") {
        name_valid = true;
    } else {
        name_valid = false;
    }
    check_status();
});

$("#password").change(function () {
    if(document.getElementById('password').value !== "") {
        password_valid = true;
    } else {
        password_valid = false;
    }
    check_status();
});

$("#email").change(function () {
    var email_value = document.getElementById('email').value;
    if(email_value == '') {
        email_valid = false;
        check_status();
        return false;
    }

    $.ajax({
        url: '/subs/validate-email/',
        data: {
            'email': email_value
        },
        dataType: 'json',
        success: function(data) {
            if (data.is_taken || !data.is_valid) {
                email_check = false;
                alert('E-mail sudah terdaftar atau tidak valid')
            } else {
                email_check = true;
                alert('E-mail belum terdaftar')
            }
            check_status();
        }
    });
});

$("#button_subs").change(function () {
    if (name_valid && password_valid && email_valid) {
        var name = document.getElementById("form_name").value;
        var pass = document.getElementById("form_password").value;
        var email = document.getElementById("form_email").value;

        $.ajax({
            type: "POST",
            url: "/subs/submit_form/",
            data: {
                'name': name,
                'password': pass,
                'email': email,
                'csrfmiddlewaretoken': '{{scrf_token}}',
            },
            dataType: 'json',
            success: function (data) {
                if (data.success_make_object) {
                    $("name").val.("");
                    $("email").val.("");
                    $("password").val.("");
                    name_valid = false;
                    email_valid = false;
                    password_valid = false;
                    $("#button_subs").attr("disabled", "disabled");
                    alert(data.msg);
                } else {
                    alert("Eror");
                }
            }
        });
    } else {
        return false;
    }
})