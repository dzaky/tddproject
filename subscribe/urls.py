from subscribe.views import show_subscribe_form
from subscribe.views import validate_email
from subscribe.views import submit_form
from django.urls import path, include 

urlpatterns = [
    path('validate-email/', validate_email, name='validate-email'),
    path('submit-form/', submit_form, name='submit-form'),
    path('', show_subscribe_form, name="show-subscribe-form"),
]
