
from django.shortcuts import render
from django.http import JsonResponse
from .models import Subscriber
from .forms import FormRegistrasi

response = {}

# Create your views here.
def show_subscribe_form(request):
    form = FormRegistrasi(request.POST)
    daftarakun = Subscriber.objects.all().values()
    return render(request, 'subscribe.html', {'form':form, 'daftarakun':daftarakun})

def validate_email(request):
    email = request.GET.get('email', None)
    is_valid = True
    if "@" not in email:
        is_valid = False

    data = {
        'is_taken': Subscriber.objects.filter(email__iexact=email).exists(),
        'is_valid': is_valid,
    }
    return JsonResponse(data)


def submit_form(request):
    message = ''
    print(request.method)

    if request.method == 'POST':
        name = request.POST.get('name', None)
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        account = Subscriber(name = name, email = email, password = password)
        account.save()
        print("SAVE?")
        message = 'data berhasil disimpan'
        success = True

    data = {
        'message': message,
        'success_make_object': success,
    }
    return JsonResponse(data)